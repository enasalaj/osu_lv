import pandas as pd
import matplotlib . pyplot as plt
data = pd . read_csv ('data_C02_emission.csv')

#koliko mjerenja sadrzi file? 
print (len(data))

#kojeg je tipa svaka velicina?
print(data.info())

#postoje li izostale vrijednosti? 
print(data.isnull().sum())
data.dropna(axis=0)

#postoje li duplikati?
print(data.duplicated())
data.drop_duplicates()

#konvertiranje u categoty
data[data.select_dtypes(['object']).columns] = data.select_dtypes(['object']).apply(lambda x: x.astype('category'))
print(data.info())

#tri auta s najvećom gradskom potrošnjom
print(data.nlargest(3,'Fuel Consumption City (L/100km)'))

#tri auta s najmanjom graskom potrošnjom
print(data.nsmallest(3,'Fuel Consumption City (L/100km)'))

#broj vozila koji imaju prosječnu veličinu motora između 2.5 i 3.5
print(len(data[(data['Engine Size (L)']>2.5 ) & (data['Engine Size (L)'] < 3.5)]))

#mjerenja s proizvođačem audi
print("Audi:")
print(len(data[(data['Make'] == 'Audi')]))

#Kolika je prosječna emisija C02 plinova automobila proizvodaca Audi koji imaju 4 cilindara?
audiCarsWithFourCylinders = data[(data['Cylinders'] == 4) & (data['Make'] == 'Audi')]
averageCO2emission = audiCarsWithFourCylinders['CO2 Emissions (g/km)'].mean()
print(averageCO2emission)

#vozilo s parnim brojem cilindara
carsWithEvenCylinders = data[(data['Cylinders'] % 2 == 0 )]
print(carsWithEvenCylinders['CO2 Emissions (g/km)'].mean())

#f
dieselCars = data[(data['Fuel Type'] == 'D')]
print('Diesel cars city consumption:')
dieselCars_cityConsumption = dieselCars['Fuel Consumption City (L/100km)'].mean()
print(dieselCars_cityConsumption)
print(dieselCars['Fuel Consumption City (L/100km)'].median())
regularGasoline = data[(data['Fuel Type'] == 'X')]
print('Regular gasoline cars city consumption:')
regularGasoline_cityConsumption = regularGasoline['Fuel Consumption City (L/100km)'].mean()
print(regularGasoline_cityConsumption)
print(regularGasoline['Fuel Consumption City (L/100km)'].median())

#g
carWithFourCylinders = data[(data['Cylinders'] == 4)]
car = carWithFourCylinders[data['Fuel Type'] == 'D']
print(car.nlargest(1,'Fuel Consumption City (L/100km)'))

#h
carsWithStick = data[(data['Transmission'].str.contains('M'))]
print(len(carsWithStick))

#i
print(data.corr( numeric_only = True ) )