import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# ucitaj sliku
img = Image.imread("lv7/test_6.jpg")

# prikazi originalnu sliku
plt.figure(1)
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w, h, d = img.shape
img_array = np.reshape(img, (w*h, d))

# kmeans


# rezultatna slika
img_array_aprox = img_array.copy()

km = KMeans(n_clusters=10, init='random', n_init=5, random_state=0)
km.fit(img_array_aprox)
labels = km.predict(img_array_aprox)
cluster_centers = km.cluster_centers_

plt.figure(2)
plt.imshow(cluster_centers[labels].reshape(w, h, d))
plt.show()

inertias = km.inertia_
plt.figure(3)
plt.plot(inertias)
plt.xlabel('Values of K')
plt.ylabel('Inertia')
plt.title('The Elbow Method using Inertia')
plt.show()
