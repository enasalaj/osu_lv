
while True:
   try:
       broj = float(input('Upisi broj: '))
   except ValueError: 
       print('Nije broj')
   else:
       if broj>= 0.0 or broj<=1.0: # this is faster
           break
       else:
           print ('Out of range. Try again')

if(broj >=0.9):
    print("A")
elif(broj >=0.8 and broj < 0.9):
    print("B")
elif(broj>=0.7 and broj < 0.8):
    print("C")
elif(broj>=0.6 and broj < 0.7):
    print("D")
else:
    print("F")