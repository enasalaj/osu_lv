listaBrojeva = []
while True:
    broj = input('Unesite broj ili upisite Done ako ste gotovi: ')
    if broj == "Done":
        break
    try:
        broj = int(broj)
        listaBrojeva.append(broj)
    except ValueError:
        print("Morate unijeti cijeli broj ili 'Done' ako ste gotovi.")
        
print(listaBrojeva)
print("Duljina liste: ")
print(len(listaBrojeva))
print("Max:")
print(max(listaBrojeva))
print("Min:")
print(min(listaBrojeva))

zb=0

for i in range(len(listaBrojeva)):
    zb+=int(listaBrojeva[i])

print("Srednja vrijednost:")
print(zb/len(listaBrojeva))