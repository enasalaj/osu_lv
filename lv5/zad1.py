from sklearn import datasets
from sklearn.metrics import ConfusionMatrixDisplay, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import OneHotEncoder
import sklearn.linear_model as lm
import matplotlib.pyplot as plt


X, y = datasets.make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)


#plt.scatter(X_train[:,0],X_train[:,1],s=50, c = y_train,edgecolors='black', cmap = 'Blues')
#plt.scatter(X_test[:,0],X_test[:,1],s=100, c = y_test,edgecolors='black', marker = "*", cmap = 'Oranges')
plt.xlabel('x1') 
plt.ylabel('x2')
#plt.show()

#2
# inicijalizacija i ucenje modela logisticke regresije
LogRegression_model = lm.LogisticRegression ()
LogRegression_model.fit(X_train , y_train)
y_test_p = LogRegression_model . predict ( X_test )
disp = ConfusionMatrixDisplay ( confusion_matrix ( X_train , X_test))
disp . plot ()
plt . show ()
